package com.example.demo.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class RedisConfiguration {

//    @Autowired
//    JedisConnectionFactory jedisConnectionFactory;

    @Bean
    public RedisTemplate<String, FileUploadToken> redisTemplate(RedisConnectionFactory _redisConnectionFactory) {
        RedisTemplate<String, FileUploadToken> template = new RedisTemplate<>();
        template.setConnectionFactory(_redisConnectionFactory);
//        template.setConnectionFactory(jedisConnectionFactory);

        /**
         * option-1
         * its operation SET & GET is the same as option-2
         * However, it's difficult to read & get in redis-cli because of key is set new Jackson2JsonRedisSerializer<>(String.class)
         * for example,
         * HGETALL "\"fileUploadToken\""
        */
//        template.setKeySerializer(new Jackson2JsonRedisSerializer<>(String.class));
//        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(FileUploadToken.class));
//        template.setHashKeySerializer(new Jackson2JsonRedisSerializer<>(String.class));
//        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(FileUploadToken.class));


        /**
         * option-2
         * its operation SET & GET is the same as option-1
         * Moreover, it's easier to read & get in redis-cli because of key is set new StringRedisSerializer()
         * for example,
         * HGETALL fileUploadToken
         */
        template.setKeySerializer(new StringRedisSerializer());
        template.setValueSerializer(new Jackson2JsonRedisSerializer<>(FileUploadToken.class));
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new Jackson2JsonRedisSerializer<>(FileUploadToken.class));

        return template;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class FileUploadToken {

        @JsonProperty(value = "fileUploadToken")
        private List<String> values = new ArrayList<>();
    }

}
