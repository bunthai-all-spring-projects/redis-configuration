package com.example.demo;

import com.example.demo.configuration.RedisConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Optional;

@SpringBootApplication
@RestController
public class DemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }


    @Autowired
    RedisTemplate<String, RedisConfiguration.FileUploadToken> redisTemplate;

    @PostMapping
    public RedisConfiguration.FileUploadToken post() {
        redisTemplate.opsForHash().put("fileUploadToken", "account_1", new RedisConfiguration.FileUploadToken(Arrays.asList("123", "321")));
        return read();
    }

    @PostMapping("/add")
    public RedisConfiguration.FileUploadToken add() {
        RedisConfiguration.FileUploadToken fileUploadToken = read();
        fileUploadToken.getValues().add("add-" + fileUploadToken.getValues().size());
        redisTemplate.opsForHash().put("fileUploadToken", "account_1", fileUploadToken);
        return read();
    }


    @PostMapping("/remove/{key}")
    public RedisConfiguration.FileUploadToken remove(@PathVariable("key") String key) {
        RedisConfiguration.FileUploadToken fileUploadToken = read();
        fileUploadToken.getValues().remove(key);
        redisTemplate.opsForHash().put("fileUploadToken", "account_1", fileUploadToken);
        return read();
    }

    public RedisConfiguration.FileUploadToken read() {
        RedisConfiguration.FileUploadToken fileUploadToken = ((RedisConfiguration.FileUploadToken) redisTemplate.opsForHash().get("fileUploadToken", "account_1"));
        if (fileUploadToken == null)
            fileUploadToken = new RedisConfiguration.FileUploadToken();
        return fileUploadToken;
    }

}
